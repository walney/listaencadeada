import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import junit.framework.Assert;

public class TesteTesteLista {
	@Test
	void testTamanho() {
		Lista lis= new Lista();
		assertEquals(0, lis.tamanho());
		
	}
	
	@Test
	void testTamanho2() {
		Lista lis= new Lista();
		Conta con=new Conta();
		lis.inserirFinal(con);
		assertEquals(1, lis.tamanho());
		
	}
	@Test
	void testTamanho3() {
		Lista lis= new Lista();
		Conta con=new Conta();
		lis.inserirFinal(con);
		lis.inserirFinal(con);
		lis.inserirFinal(con);

		assertEquals(3, lis.tamanho());
		
	}
	@Test
	void testTamanho4() {
		Lista lis= new Lista();
		Conta con=new Conta();
		lis.inserirFinal(con);
		lis.inserirFinal(con);
		lis.inserirFinal(con);
		lis.inserirFinal(con);

		assertEquals(4, lis.tamanho());
		
	}
	

	

}
public class Lista {
	Conta inicio;
	int inseridos;
	public void inserirFinal(Conta elemento) {
		if(inicio==null) {
			inicio=elemento;
			
			
		}
		else {
			Conta temp=inicio;
			while(temp.prox!=null) {
				temp=temp.prox;
			}
			temp.prox=elemento;
		}
		inseridos++;
	}
	public int tamanho() {
		return inseridos;
	}
	
	

}
